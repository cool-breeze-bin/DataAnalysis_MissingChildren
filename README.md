# DataAnalysis_MissingChildren

#### 介绍
失踪儿童数据分析（github同步）

数据来源：各大官网或公益性组织公开数据

### 预览数据大屏
链接：[http://cool-breeze-bin.gitee.io/dataanalysis_missingchildren/](http://cool-breeze-bin.gitee.io/dataanalysis_missingchildren/)

说明：内含视频，故需等待

地理位置分布情况研究，可单独参考：[ http://cool-breeze-bin.gitee.io/data-analysis-competition]( http://cool-breeze-bin.gitee.io/data-analysis-competition)
#### 软件架构
主要使用前端知识以及pyecharts对失踪儿童数据进行可视化分析

#### 安装教程

使用git或github desktop  Clone此工程[https://gitee.com/cool-breeze-bin/DataAnalysis_MissingChildren.git](https://gitee.com/cool-breeze-bin/DataAnalysis_MissingChildren.git)

#### 使用说明

下载此代码后，预览或编辑index.html即可，尽量使用Google Chrome打开

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_Name(你的姓名拼音) 分支
3.  提交代码
4.  新建 Pull Request

